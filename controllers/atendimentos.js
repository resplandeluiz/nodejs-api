const Atendimento = require('../models/atendimentos')

module.exports = app => {

    app.get('/atendimentos/:id', (req, res) => {

        const id = parseInt(req.params.id)

        Atendimento.buscaPorId(id)
            .then(response => {

                if (response == null) {
                    res.status(204).json()
                } else {
                    res.json(response)
                }

            })
            .catch(e => res.status(400).json(e))

    })

    app.delete('/atendimentos/:id', (req, res) => {

        const id = parseInt(req.params.id)

        Atendimento.deletar(id)
            .then(response => res.json(response))
            .catch(e => res.status(400).json(e))

    })

    app.get('/atendimentos', (req, res) => {

        Atendimento.listar()
            .then(atendimentos => res.json(atendimentos))
            .catch(e => res.status(400).json(e))

    })

    app.post('/atendimentos', (req, res) =>

        Atendimento.adicionar(req.body)
        .then(atendimentoCadastrado => res.status(201).json(atendimentoCadastrado))
        .catch(e => res.status(400).json(e))

    )

    app.patch('/atendimentos/:id', (req, res) => {

        const id = parseInt(req.params.id)
        const valores = req.body
        Atendimento.alterar(id, valores)
            .then(atualizado => res.status(200).json(atualizado))
            .catch(e => res.status(400).json(e))

    })

}