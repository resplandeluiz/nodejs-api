const customExpress = require('./config/customExpress')
const conexao = require('./infra/database/conexao')
const Tabelas = require('./infra/database/tabelas')

conexao.connect(error => {
    if (error) console.warn(error)
    else {

        Tabelas.init(conexao)

        const app = customExpress()
        app.listen(3000, () => console.log("servidor rodando na porta 3000"))

    }
})