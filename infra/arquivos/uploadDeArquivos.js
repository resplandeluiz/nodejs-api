const fs = require('fs')
const path = pathLib = require('path')

const PATH_SAVE = './assets/imagens/'

module.exports = (path, nomeArquivo, callbackImagemCriada) => {

    const tipoValidos = ['jpg', 'png', 'jpeg']

    const tipo = pathLib.extname(path)
    const tipoEhValido = tipoValidos.indexOf(tipo.substring(1)) !== -1   

    if (tipoEhValido) {

        const completePath = `${PATH_SAVE}${nomeArquivo}${tipo}`

        fs.createReadStream(path)
            .pipe(fs.createWriteStream(completePath))
            .on('error', (e) => console.log(e))
            .on('finish', () => { callbackImagemCriada(false, completePath) })


    } else {

        const erro = { mensagem: "Tipo da imagem é invalido" }
        callbackImagemCriada(erro)

    }

}