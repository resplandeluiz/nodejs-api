class Tabelas {

    init(conexao) {
        this.conexao = conexao
        this.criarAtendimentos()
        this.criarPets()
    }

    runQuery(query) { this.conexao.query(query, (error) => { if (error) console.log(error) }) }

    criarAtendimentos() {
        const sqlQuery = `CREATE TABLE IF NOT EXISTS atendimentos (id int NOT NULL AUTO_INCREMENT, cliente varchar(11) NOT NULL, pet varchar(20), servico varchar(20) NOT NULL, status varchar(20) NOT NULL, observacoes text, PRIMARY KEY(id), data datetime NOT NULL,dataCriacao datetime NOT NULL )`
        this.runQuery(sqlQuery)
    }


    criarPets() {
        const sqlQuery = `CREATE TABLE IF NOT EXISTS pets (id int NOT NULL AUTO_INCREMENT, nome varchar(50) NOT NULL, imagem varchar(200), PRIMARY KEY(id) )`
        this.runQuery(sqlQuery)
    }

}


module.exports = new Tabelas