const axios = require('axios')
const moment = require('moment')
const conexao = require('../infra/database/conexao')
const repositorio = require('../repositorios/atendimento')

class Atendimento {


    constructor() {

        this.dataValida = ({ data, dataCriacao }) => moment(data).isSameOrAfter(dataCriacao)
        this.clienteNomeValido = (sizeNome) => sizeNome >= 5

        this.validacoes = [
            { nome: "data", mensagem: "Data deve ser maior o igual a data de criação.", valido: this.dataValida },
            { nome: "cliente", mensagem: "Nome deve ter pelo menos 5 caracteres.", valido: this.clienteNomeValido },
        ]


        this.validar = params => this.validacoes.filter(campo => {

            const { nome } = campo
            const parametro = params[nome]
            return !!campo.valido(parametro)

        })


    }

    buscaPorId(id) {
        return repositorio.buscaPorId(id)
            .then(async atendimentoArray => {

                if (atendimentoArray.length == 0) {
                    return null
                } else {
                    const atendimento = atendimentoArray[0]
                    const cpf = atendimento.cliente

                    const { data } = await axios.get(`http://localhost:8082/${cpf}`)

                    atendimento.cliente = data

                    return atendimento
                }

            })
    }


    listar() {
        return repositorio.listar()
            .then(resultado => resultado)
    }

    adicionar(atendimento) {

        const data = moment(atendimento.data, "DD/MM/YYYY").format("YYYY-MM-DD HH:MM:SS")
        const dataCriacao = moment().format("YYYY-MM-DD HH:MM:SS")

        const params = {
            data: { data, dataCriacao },
            cliente: { nome: atendimento.cliente.length }
        }

        const erros = this.validar(params)
        const existemErros = erros.length

        if (existemErros) {

            return new Promise((resolve, reject) => reject(erros))

        } else {

            const atendimentoDatato = {...atendimento, dataCriacao, data }

            return repositorio.adicionar(atendimentoDatato)
                .then(resultado => {
                    const id = resultado.isertId
                    return {...atendimento, id }
                })

        }

    }


    alterar(id, valores) {
        if (valores.data) valores.data = moment(valores.data, "DD/MM/YYYY").format("YYYY-MM-DD HH:MM:SS")

        return repositorio.alterar({...valores, id })
            .then(resultado => resultado)
    }


    deletar(id) {
        return repositorio.deletar(id)
            .then((response) => response)
    }


}


module.exports = new Atendimento