const conexao = require('../infra/database/conexao')
const uploadDeArquivo = require('../infra/arquivos/uploadDeArquivos')
const repositorio = require('../repositorios/pets')

class Pet {

    adicionar(pet) {
        return new Promise((resolve, reject) => {

            uploadDeArquivo(pet.imagem, pet.nome, (erro, completePath) => {

                if (erro) {
                    return reject(erro)

                } else {

                    const novoPet = { nome: pet.nome, imagem: completePath }

                    return repositorio.adicionar(novoPet)
                        .then(resultado => {
                            return resolve({ ...novoPet, id: resultado.insertId })
                        })
                }
            })
        })


    }

}



module.exports = new Pet