const query = require('../infra/database/queryes')

class DataDefault {

    constructor(tableName) {
        this.tableName = tableName
    }

    adicionar(atendimento) {
        const sql = `INSERT INTO ${this.tableName} SET ?`
        return query(sql, atendimento)
    }


    listar() {
        const sql = `SELECT * FROM ${this.tableName}`
        return query(sql)
    }


    buscaPorId(id) {
        const sql = `SELECT * FROM ${this.tableName} WHERE id=?`
        return query(sql, id)
    }


    deletar(id) {
        const sql = `DELETE FROM ${this.tableName} WHERE id=?`
        return query(sql, id)
    }


    alterar(valores) {

        const sql = `UPDATE ${this.tableName} SET ? WHERE id=?`
        return query(sql, valores)

    }


}


module.exports = DataDefault